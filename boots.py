# -*- coding: utf-8 -*-
"""
Created on Mon Oct  7 09:09:17 2019

My bootstraping codes

@author: gomel
"""
import numpy as np
#import matplotlib.pyplot as plt
import scipy.stats as stats
#import metrics
#from sklearn.utils import resample
from time import sleep
import sys
#import astropy.stats as astrost
#from numba import jit,njit,prange
#from time import time

#@jit(nopython=True,parallel=True)
#@jit(parallel=True)
def reesample(x): #simple resample of data. 
    '''this code produces a simple resample of the array x, and of the same size as x'''
    total_data=len(x)    
    sampled=np.empty((total_data),dtype='float64')
    for n in range(total_data):
        idx=stats.uniform.rvs(size=total_data,scale=total_data)
        sampled[n]=x[int(idx[n])]
    return sampled


#@jit(parallel=True)
def bootstrap_func_len(fun,s,number_of_samples,**debug):  
    '''this code does simple non parametric bootstraping of fixed lenght'''
     
    '''
    -'fun' is the input function for an statistic, for example np.mean
    -'s' is an array of the original sample. It's better if it's a numpy array
    -'number_of_samples' is the amount of samples to be done for bootstraping (int)
    
    -'debug' if set to True prints a progress bar
    
    The code return:
        boot_stat: 1D np.array of the resulting stat after each resample
        boot_error: 1D np.array of the distance betwee each result of the resampling 
                and the empirical statistic
    '''
    boot_stat=np.empty((number_of_samples),dtype='float64')
    boot_error=np.empty((number_of_samples),dtype='float64')
    boot_deviation=np.empty((number_of_samples),dtype='float64')
    percent=33#percentaje for the error 

    real_stat=fun(s)
    for n in range(number_of_samples):
        boot = reesample(s)
        boot_stat[n]=fun(boot)
        boot_deviation[n]=boot_stat[n]-real_stat        
        if debug==True:
            ####progress bar###
            sys.stdout.write('\r')
            # the exact output you're looking for:
            sys.stdout.write("Bootstraping with %.i samples: [%-10s] %d%%" \
                             % (number_of_samples,'='*int(0.1*n*100/number_of_samples), n*100/number_of_samples))
            sys.stdout.flush()
            sleep(0.01)
            ####end progress bar###
    boot_error=[np.percentile(boot,percent),np.percentile(boot,100-percent)] #sorts x*-<x> increasingly to take the percentile.    
  
    return boot_stat, boot_error        

####################################
def bootstrap_func_len2(fun,s,number_of_samples,**debug):  
    '''this code does simple non parametric bootstraping of fixed lenght'''
     
    '''
    -'fun' is the input function for an statistic, for example np.mean
    -'s' is an array of the original sample. It's better if it's a numpy array
    -'number_of_samples' is the amount of samples to be done for bootstraping (int)
    
    -'debug' if set to True prints a progress bar
    
    The code return:
        boot_stat: 1D np.array of the resulting stat after each resample
        boot_error: 1D np.array of the distance betwee each result of the resampling 
                and the empirical statistic
    '''
    boot_stat=np.empty((number_of_samples),dtype='float64')
    # boot_error=np.empty((number_of_samples),dtype='float64')
    boot_deviation=np.empty((number_of_samples),dtype='float64')
    # percent=33#percentaje for the error 

    real_stat=fun(s)
    for n in range(number_of_samples):
        boot = reesample(s)
        boot_stat[n]=fun(boot)
        boot_deviation[n]=boot_stat[n]-real_stat        
        if debug==True:
            ####progress bar###
            sys.stdout.write('\r')
            # the exact output you're looking for:
            sys.stdout.write("Bootstraping with %.i samples: [%-10s] %d%%" \
                             % (number_of_samples,'='*int(0.1*n*100/number_of_samples), n*100/number_of_samples))
            sys.stdout.flush()
            sleep(0.01)
            ####end progress bar###
    return boot_stat, boot_deviation    


################################################
def blockbootstrap(fun,s,nsamples,perc=70,rep=True,sd=None,**debug):  
    '''this code does simple non parametric bootstraping of fixed lenght'''
     
    '''
    -'fun' is the input function for an statistic, for example np.mean
    -'s' is an array of the original sample. It's better if it's a numpy array
    -'number_of_samples' is the amount of samples to be done for bootstraping (int)
    
    -'debug' if set to True prints a progress bar
    
    The code return:
        boot_stat: 1D np.array of the resulting stat after each resample
        boot_error: 1D np.array of the distance betwee each result of the resampling 
                and the empirical statistic
    '''
    boot_stat=np.empty((nsamples),dtype='float64')
    # boot_error=np.empty((number_of_samples),dtype='float64')
    boot_deviation=np.empty((nsamples),dtype='float64')
    # percent=33#percentaje for the error 

    real_stat=fun(s)
    rng = np.random.default_rng(seed=sd)
    for n in range(nsamples):
        boot = rng.choice(s,size=np.int64(len(s)*perc/100),replace=rep)
        #boot = boot[0:np.int64(len(s)*perc/100)]
        boot_stat[n]=fun(boot)
        boot_deviation[n]=boot_stat[n]-real_stat        
        if debug==True:
            ####progress bar###
            sys.stdout.write('\r')
            # the exact output you're looking for:
            sys.stdout.write("Bootstraping with %.i samples: [%-10s] %d%%" \
                             % (nsamples,'='*int(0.1*n*100/nsamples), n*100/nsamples))
            sys.stdout.flush()
            sleep(0.01)
            ####end progress bar###
            '''include small sample error'''
    return boot_stat, boot_deviation    
#################################################33
def bootstraping_func_dinamical( fun, s ,confidence,rel_error_tolerance,**debug):
    '''Non parametric simple bootstraping with autom sample test'''
    ''' This code uses simple bootstrapping and does resamples until the 
    errors go below some tolerance threshold and are monotonically decreading for 
    several iterations'''
    
    '''
    -'fun' is the input function for an statistic, for example np.mean
    -'s' is an array of the original sample. It's better if it's a numpy array
    -'confidence' is the porcentage confidence interval desired, for example, 
    an interval of 95%% confidence.
    -rel_error_tolerance is the tolerance error threshold for the code
    
    -debug. If debug it set to TRUE, aditional information is printed. 
     
    The code return:
        boot_stat: 1D np.array of the resulting stat after each resample
        boot_error: 1D np.array of the distance betwee each result of the resampling 
                and the empirical statistic
                
        conf_index: tuple, with the index values for the desired confidence interval
    '''
     
    ##################internal parameters############################
    consecutive_error=6
    min_sample_number=20
    ##################end of internal parameters

    real_stat=fun(s) #this is the empirical statistic.   
    number_of_samples = 0
    rel_error_right=100
    rel_error_left=100
    error_left=1000
    error_right=1000
    conf_interval=(0.5-0.01*confidence/2,.5+0.01*confidence/2)
    if debug==True:  print(conf_interval)
    boot_stat=[] #i could alocate some nparray of dim 50 to make it more efficient maybe
    boot_error=[]
        
    error_count=0
    
    while error_count<consecutive_error:
        number_of_samples+=1
        conf_index=(int(number_of_samples*conf_interval[0]),int(number_of_samples*conf_interval[1]))
#        print('idx',conf_index)
        boot = reesample(s)
        boot_stat.append(fun(boot))
        boot_error.append(boot_stat[-1]-real_stat)
        boot_error.sort() #sorts x*-<x> increasingly to take the percentile.
       
        if number_of_samples>min_sample_number:       
            mean_boot=np.mean(boot_stat)
#            mean_error=abs(real_stat-mean_boot)           
            rel_error_left=abs(error_left-boot_error[conf_index[0]])
            rel_error_right=abs(error_right-boot_error[conf_index[1]])
            error_left=boot_error[conf_index[0]]
            error_right=boot_error[conf_index[1]]

            if rel_error_right<rel_error_tolerance:
                if rel_error_left<rel_error_tolerance:
                    if abs(mean_boot-real_stat)<0.1*real_stat:     
                        error_count+=1
                        print('\n error count: ',error_count,' \n ' )
            else : error_count=0   
            
        error_left=boot_error[conf_index[0]]
        error_right=boot_error[conf_index[1]]
#            else : error_count=0
    #    print('error= ', boot_error[-1],'\n relative error: ' , rel_error)
        '''progress bar'''
        sys.stdout.write('\r')
        # the exact output you're looking for:
        sys.stdout.write("number of samples: %.i, error on the right:  %.3f, relative error on the right: %.3f" \
                         % ( number_of_samples,error_right , rel_error_left )  )
    #    sys.stdout.write("number of samples: %.i \n error:  %.3f \n relative error: %.3f \n" \
    #                     % ( number_of_samples,error , rel_error )  )
        sys.stdout.flush()
        sleep(0.0001)
        
        if number_of_samples>700: 
            print('\n After 700 samples the mean is still too far away.')
            break
        
    return boot_stat, boot_error, conf_index

############## 
def boot_result(boot_stat,normality=False):
    high=np.percentile(boot_stat,95)
    low=np.percentile(boot_stat,5)
    mean=np.mean(boot_stat)
    
    if normality==True:
        k2, p = stats.normaltest(boot_stat)
        alpha = 1e-3
        print("p = {:g}".format(p))
        if p < alpha:
            # null hypothesis: x comes from a normal distribution
            print("The null hypothesis can be rejected")
            nt=0
        else:
            print("The null hypothesis cannot be rejected")
            nt=1
    return mean, [high, low]

