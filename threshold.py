# -*- coding: utf-8 -*-
"""
Created on Wed May 16 15:06:11 2018

Module for definitions of Extreme events Thresholds.

@author: gomel
"""
import numpy as np


def sig_wave_m0(x):
# =============================================================================
#     Extreme wave is anything beyond 2 significant wave hights.
#     Def of 'significant Wave height (m0)': 4*std dev.     
# =============================================================================
    
    Hm0=4*np.std(x)
    
    return Hm0


def sig_wave_3(x):
# =============================================================================
#     Extreme wave is anything beyond 2 significant wave hights.
#     Def of 'significant Wave height': average of the last third of the wave heights
# =============================================================================
    lim=np.max(x)*2/3 + np.min(x)*1/3
    suma=0.
    i=0
#    print(lim)
    for j in x:
        if j>lim:
            suma=suma+j
            i=i+1
    
    H3=suma/i
    return H3

def sig_wave_3sort(x):
# =============================================================================
#     Extreme wave is anything beyond 2 significant wave hights.
#     Def of 'significant Wave height': average of the last third of the wave heights
#     THIS IS THE DEFINITION THAT GIVES 1/3000 FOR RAYLEIGH
# =============================================================================
    x2=np.copy(x)
    x2[::-1].sort()
    if x2[2]>x2[1]:
        print('order error')
        return
    t=np.int(len(x2)/3)
    H3=np.mean(x2[:t])
#    print(x[t])

    return H3


def sig_wave_rms(x):
# =============================================================================
#     Extreme wave is anything beyond 2 significant wave hights.
#     Def of 'significant Wave height': 1.4*RMS(H)
# =============================================================================    
    Hrms=1.4*np.sqrt(np.mean(x**2))
    return Hrms

def symtr(x):
    # =============================================================================
    #     Extreme wave is antyhing above 'high' with is factor*std deviation of the highest half 
    #     of the distribution. Same for extremely rare low events and 'low'. 
    # =============================================================================    

    factor=3.69150749571975

    mean=np.mean(x) # mean 
    newdata=[]
    for data in x:
        if data>mean:
            newdata.append(data)
            newdata.append(2*mean-data)
    var_max=np.std(newdata)        
    high=mean+factor*var_max

    newdata=[]
    for data in x:
        if data<mean:
            newdata.append(data)
            newdata.append(2*mean-data)
    var_min=np.std(newdata)        
    low=mean-factor*var_min

    return (low,high)