# -*- coding: utf-8 -*-
"""
Created on Tue May 15 16:56:49 2018

It needs 2d array input implementation with axis choice
@author: gomel
"""


import numpy as np
import threshold

def metricm(x,k=0.2,ax=None):
# =============================================================================
#       Code for the pareto-like Metric from Jerome and Maura. As used on the paper 
#   	'optical rogue waves statistics in laser filamentation. 
#    
#    to be used only on positive defines distributions of data. 
# =============================================================================
    x2=np.sort(x,axis=ax)  
    if ax==None :
        filt=np.take(x2,indices = np.arange(len(x2)-np.int64(k*len(x2)),len(x2),1),axis=ax)        
    else:
        filt=np.take(x2,indices = np.arange(np.shape(x2)[ax]-np.int64(k*np.shape(x2)[ax]),np.shape(x2)[ax],1),axis=ax)                    
    tail=np.sum(filt,axis=ax)
    total=np.sum(x2,axis=ax)
    M=tail/total   
    return M

def RTW_high(x,k=0.2,ax=None):
    # =============================================================================
#       Code for the pareto-like Metric from Jerome and Maura. As used on the paper 
#   	'optical rogue waves statistics in laser filamentation. 
#    
#    to be used only on positive defines distributions of data. 
#       -This code moves the distribution to make it tranlationally invariant. 
# =============================================================================
    x2=np.sort(x,axis=ax) 
    x2=x2-np.take(x2,indices = [0],axis=ax) #moves everything to 0 along the axis.
    if ax==None :
       # print('none axis')
        filt=np.take(x2,indices = np.arange(len(x2)-np.int64(k*len(x2)),len(x2),1),axis=ax)        
    else:
        filt=np.take(x2,indices = np.arange(np.shape(x2)[ax]-np.int64(k*np.shape(x2)[ax]),np.shape(x2)[ax],1),axis=ax)                    
    tail=np.sum(filt,axis=ax)
    total=np.sum(x2,axis=ax)
    M=tail/total
    return M

def RTW_high_abs(x,k=0.2,ax=None):
    # =============================================================================
#       Code for the pareto-like Metric from Jerome and Maura. As used on the paper 
#   	'optical rogue waves statistics in laser filamentation. 
#    
#    to be used only on positive defines distributions of data. 
#       -This code moves the distribution to make it tranlationally invariant. 
# =============================================================================
    x2=np.sort(x,axis=ax) 
    if ax==None :
        filt=np.take(x2,indices = np.arange(len(x2)-np.int64(k*len(x2)),len(x2),1),axis=ax)        
    else:
        filt=np.take(x2,indices = np.arange(np.shape(x2)[ax]-np.int64(k*np.shape(x2)[ax]),np.shape(x2)[ax],1),axis=ax)                    
    tail=np.sum(filt,axis=ax)
    total=np.sum(x2,axis=ax)
    M=tail/total   
    return M

def RTW_low(x,k=0.2,ax=None):
# =============================================================================
#       Code for the pareto-like Metric from Jerome and Maura. As used on the paper 
#   	'optical rogue waves statistics in laser filamentation. 
#    
#    to be used only on positive defines distributions of data. 
#       -This code moves the distribution to make it tranlationally invariant. 
# =============================================================================
    x2=np.sort(x,axis=ax)    
    x2=x2-np.take(x2,indices = [0],axis=ax)#make it possitive
    '''here i turn it around'''  
    x2=np.flip(x2,axis=ax)
    x2=x2-np.take(x2,indices = [0],axis=ax)#make it possitive
    if ax==None :
     #  print('none axis')
       filt=np.take(x2,indices = np.arange(len(x2)-np.int64(k*len(x2)),len(x2),1),axis=ax)        
    else:
      # print('along some axis')
      # print('along some axis')
       filt=np.take(x2,indices = np.arange(np.shape(x2)[ax]-np.int64(k*np.shape(x2)[ax]),np.shape(x2)[ax],1),axis=ax)                    
    tail=np.sum(filt,axis=ax)
    total=np.sum(x2,axis=ax)
    M=tail/total  
    return M

def RTW_avg(x,k=0.2,axis=None):
    M=.5*(RTW_high(x,k,ax=axis)+RTW_low(x,k,ax=axis))
    return M

def RTW_max(x,k=0.2,axis=None):
# =============================================================================
#Takes the maximum between RTW_high and RTW_low. Default value for k is 0.2.
# =============================================================================
    M=np.max((RTW_high(x,k,ax=axis),RTW_low(x,k,ax=axis)),axis=axis)
    return M

def RTW_c(x,k=0.2,axis=None):
# =============================================================================
# Complex metrics with RTW_high and RTW_low. Default value for k is 0.2.
# =============================================================================
    a=RTW_high(x,k,ax=axis)
    b=RTW_low(x,k,ax=axis)
    M=np.complex(a,b)
    return M

def metric_rri(x):
# =============================================================================
#     Extreme wave is anything beyond 2 significant wave hights.
#     Counts Extreme events density  and compares to 1/3000(the rayleight experted EE density.) 
# =============================================================================
    x2=np.copy(x)
    thres=2*threshold.sig_wave_3sort(x2)
    rw_count=len(x2[x2>thres])    
    RW=rw_count*2980.0/len(x2)
    # RW=rw_count
#    print64(rw_count)
    return RW


def metric_rc_tld(x):
# =============================================================================
#    Similar to rri: 
#     Extreme wave is anything beyond 2 significant wave hights.
#     Counts Extreme events after moving the distribution to begin at 0.   
# =============================================================================
    x2=np.copy(x)-min(x)
    thres=2*threshold.sig_wave_3sort(x2)
    rw_count=len(x2[x2>thres])    
    RW=rw_count*2980/len(x2)
    return RW


   
def metric_rms_tld(x):
# =============================================================================
#     Extreme wave is anything beyond 2 significant wave hights.
#     Counts Extreme events after moving the distribution to begin at 0.   
# =============================================================================
    x2=np.copy(x)-min(x)
    thres=2*threshold.sig_wave_rms(x2)
    rw_count=len(x2[x2>thres])    
    RW=rw_count*2980/len(x2)
    return RW

def metric_sigma(x):
# =============================================================================
#     Extreme wave is anything beyond 2 significant wave hights.
#     Counts Extreme events after moving the distribution to begin at 0.   
# =============================================================================
    x2=np.copy(x)-min(x)
    thres=2*threshold.sig_wave_m0(x2)
    rw_count=len(x2[x2>thres])    
    RW=rw_count/len(x2)
    return RW
    
    

def kr2(x,ax=None):
    x=np.array(x)
    u5=np.percentile(x,50,axis=ax)
    geq_filt=np.greater_equal(x,u5)
    geq_filt=np.ma.array(x, mask=np.logical_not(np.greater_equal(x,u5)))
    U5=geq_filt.mean(axis=ax)
    leq_filt=np.ma.array(x, mask=np.logical_not(np.less_equal(x,u5)))
    L5=leq_filt.mean(axis=ax)
    minx=np.min(x,axis=ax)
    maxx=np.max(x,axis=ax)
    KR_num=5 #lkr and ukr shuold not be the max or minimum.. for large tails and low datasets this can happen.
    ukr=np.percentile(x,100-KR_num,axis=ax)
    lkr=np.percentile(x,KR_num,axis=ax)
    #if ax==None :
    #	if minx==lkr or ukr==maxx :
    #		print64('Minimum or maximum equal to percentile error.')
    
    #this version uses <= and >= to have it well defined always. 
    geq_filt=np.ma.array(x, mask=np.logical_not(np.greater_equal(x,ukr)))
    leq_filt=np.ma.array(x, mask=np.logical_not(np.less_equal(x,lkr)))
    
    UKR=geq_filt.mean(axis=ax)
    LKR=leq_filt.mean(axis=ax)
    KR2=(UKR-LKR)/(U5-L5)
    return KR2

def kr3(x,ax=None):
    x=np.array(x)
    u5=np.percentile(x,50,axis=ax)
    geq_filt=np.greater_equal(x,u5)
    geq_filt=np.ma.array(x, mask=np.logical_not(np.greater_equal(x,u5)))
    U5=geq_filt.mean(axis=ax)
    leq_filt=np.ma.array(x, mask=np.logical_not(np.less_equal(x,u5)))
    L5=leq_filt.mean(axis=ax)
    minx=np.min(x,axis=ax)
    maxx=np.max(x,axis=ax)
    KR_num=20 #lkr and ukr shuold not be the max or minimum.. for large tails and low datasets this can happen.
    ukr=np.percentile(x,100-KR_num,axis=ax)
    lkr=np.percentile(x,KR_num,axis=ax)
    #if ax==None :
    #	if minx==lkr or ukr==maxx :
    #		print64('Minimum or maximum equal to percentile error.')
    
    #this version uses <= and >= to have it well defined always. 
    geq_filt=np.ma.array(x, mask=np.logical_not(np.greater_equal(x,ukr)))
    leq_filt=np.ma.array(x, mask=np.logical_not(np.less_equal(x,lkr)))
    
    UKR=geq_filt.mean(axis=ax)
    LKR=leq_filt.mean(axis=ax)
    KR3=(UKR-LKR)/(U5-L5)
    return KR3

def Moors(x,ax=None):
    ##########################################################################
    #kurtosis estimator as defined in kim & white 2004
    #On more robust estimation of skewness and kurtosis
    ##########################################################################
    x=np.array(x)
    E1=np.quantile(x,0.125,axis=ax)
    E2=np.quantile(x,0.25,axis=ax)
    E3=np.quantile(x,0.375,axis=ax)
    # E4=np.percentile(x,50,axis=ax)
    E5=np.quantile(x,0.625,axis=ax)
    E6=np.quantile(x,0.75,axis=ax)
    E7=np.quantile(x,0.875,axis=ax)
    M=((E7-E5)+(E3-E1))/(E6-E2)-1.23
    return M 

def krn(x,N,ax=None):
    """Generalization of the KR2 and KR3 metrics.
    Input N should be between 0 and 50"""    
    x=np.array(x)
    u5=np.percentile(x,50,axis=ax)
    geq_filt=np.greater_equal(x,u5)
    geq_filt=np.ma.array(x, mask=np.logical_not(np.greater_equal(x,u5)))
    U5=geq_filt.mean(axis=ax)
    leq_filt=np.ma.array(x, mask=np.logical_not(np.less_equal(x,u5)))
    L5=leq_filt.mean(axis=ax)
    minx=np.min(x,axis=ax)
    maxx=np.max(x,axis=ax)
    KR_num=N #lkr and ukr shuold not be the max or minimum.. for large tails and low datasets this can happen.
    ukr=np.percentile(x,100-KR_num,axis=ax)
    lkr=np.percentile(x,KR_num,axis=ax)
    #if ax==None :
    #	if minx==lkr or ukr==maxx :
    #		print64('Minimum or maximum equal to percentile error.')
    
    #this version uses <= and >= to have it well defined always. 
    geq_filt=np.ma.array(x, mask=np.logical_not(np.greater_equal(x,ukr)))
    leq_filt=np.ma.array(x, mask=np.logical_not(np.less_equal(x,lkr)))
    
    UKR=geq_filt.mean(axis=ax)
    LKR=leq_filt.mean(axis=ax)
    KRN=(UKR-LKR)/(U5-L5)
    return KRN
    
######################################################################
######################################################################


